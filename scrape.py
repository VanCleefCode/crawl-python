# -*- coding: utf-8 -*-
import urllib2
from bs4 import BeautifulSoup
import MySQLdb


#MYSQL
db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user="root",         # your username
                     passwd="1111",  # clave 1111 local your password
                     db="crawl")        # name of the data base

# you must create a Cursor object. It will let
#  you execute all the queries you need
cur = db.cursor()

# Use all the SQL you like
#cur.execute("SELECT * FROM empresas")

# print all the first cell of all the rows
#for row in cur.fetchall():
    #print row[0]




url = raw_input('Ingrasa la URL principal terminando en /: ')
inicio = int(input('Ingresa el numero de pagina a Iniciar la Extracion: '))
fin = int(input('Ingresa el numero de pagina a finalizar la Extracion: '))

for i in range (inicio,fin + 1):
	print 'PAGINA', i
	quote_page = url + '?qPagina=' + str(i)
	
	#preguntar si la pagina no existe para incluirla
	cur.execute("SELECT * FROM pagina WHERE nombre = '" +  quote_page + "'")
	data = cur.fetchall()
	if not data:
		page = urllib2.urlopen(quote_page)
		soup = BeautifulSoup(page, 'html.parser')

		table = soup.find('table', attrs={'class':'ranking_einf'})

		for name_box in table.find_all('tr'):
			for a in name_box.find_all('td', attrs={'class': 'textleft'}):
				for t in a.find_all('a'):
					url_link = 'https://guiaempresas.universia.es' + t['href']
					page_link = urllib2.urlopen(url_link)
					soup_link = BeautifulSoup(page_link, 'html.parser')
					nombre = soup_link.find('h1', attrs={'class':'h1ficha fn localbusiness'})
					direccion = soup_link.find('span', attrs={'class':'street-address'})
					localidad = soup_link.find('span', attrs={'class':'locality'})
					provincia = soup_link.find('span', attrs={'id':'situation_prov'})

					#div = soup_link.find('div', attrs={'id':'ficha_iden'})
					#ul = div.find('ul')
					telefono = None
					for strong in soup_link.find_all('strong'):
						t = strong.text.strip()
						if t.encode('ascii', 'ignore').decode('ascii') == 'Telfono:':
							telefono = strong.next_sibling
						
					
					if telefono:
						#raw_input('PRESIONA ENTER')
						print (nombre.text, localidad.text, provincia.text,telefono)
						cur.execute("INSERT INTO empresas(nombre,direccion,localidad,provincia,telefono) VALUES ('" + nombre.text + "','" + direccion.text + "','" + localidad.text + "','" + provincia.text + "','" + telefono + "')")
						db.commit()
					else:
						#raw_input('PRESIONA ENTER')
						print (nombre.text, localidad.text, provincia.text)
						cur.execute("INSERT INTO empresas(nombre,direccion,localidad,provincia) VALUES ('" + nombre.text + "','" + direccion.text + "','" + localidad.text + "','" + provincia.text + "')")
						db.commit()
		cur.execute("INSERT INTO pagina(nombre) VALUES ('" + quote_page + "')")
		db.commit()
	else:
		print "PAGINA EXTRAIDA !YA EXISTE EN LA BASE DE DATOS!"	
db.close()
